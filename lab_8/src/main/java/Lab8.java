import java.util.Scanner;
import Corporate.Corporate;

class Lab8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            int maxSalary = Integer.parseInt(input.nextLine());
            if (maxSalary > 0) {
                Corporate.setMaxSalary(maxSalary);
                System.out.println("Batas gaji maksimal STAFF: " + maxSalary);
            }
            while (input.hasNextLine()) {
                String[] input_split = input.nextLine().split(" ");
                if (input_split[0].toUpperCase().equals("TAMBAH_KARYAWAN")) {
                    Corporate.add(input_split[2], input_split[1].toUpperCase(), Integer.parseInt(input_split[3]));
                } else if (input_split[0].toUpperCase().equals("STATUS")) {
                    Corporate.status(input_split[1]);
                } else if (input_split[0].toUpperCase().equals("GAJIAN")) {
                    Corporate.gajian();
                } else if (input_split[0].toUpperCase().equals("TAMBAH_BAWAHAN")) {
                    Corporate.addBawahan(input_split[1], input_split[2]);
                } else {
                    System.out.println("FORMAT MASUKAN SALAH!");
                }
            }
        } catch (Exception e) {
            System.out.println("FORMAT MASUKAN SALAH!");
        }
    }
}