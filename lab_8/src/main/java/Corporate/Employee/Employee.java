package Corporate.Employee;

import java.util.ArrayList;

import Corporate.Employee.Manager.Manager;
import Corporate.Employee.Staff.Staff;
import Corporate.Employee.Intern.Intern;

public class Employee {
    private String name;
    private int salary;
    private int salaryCounter = 0;
    private int maxBawahan;
    private ArrayList<Employee> bawahan = new ArrayList<Employee>();
    private static final int MIN_SALARY_RECEIVED_BEFORE_RAISE = 6;

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
        if (this instanceof Staff || this instanceof Manager) {
            this.maxBawahan = 10;
        } else if (this instanceof Intern) {
            this.maxBawahan = 0;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getSalaryCounter() {
        return salaryCounter;
    }

    public void setSalaryCounter(int salaryCounter) {
        this.salaryCounter = salaryCounter;
    }

    public int getMaxBawahan() {
        return maxBawahan;
    }

    public void setMaxBawahan(int maxBawahan) {
        this.maxBawahan = maxBawahan;
    }

    public ArrayList<Employee> getBawahan() {
        return bawahan;
    }

    public void setBawahan(ArrayList<Employee> bawahan) {
        this.bawahan = bawahan;
    }

    public void addBawahan(Employee atasan) {
        bawahan.add(atasan);
    }

    public boolean canRecruit(Employee employee) {
        if (this instanceof Manager) {
            if (employee instanceof Staff || employee instanceof Intern) {
                return true;
            }
        } else if (this instanceof Staff) {
            if (employee instanceof Intern) {
                return true;
            }
        }
        return false;
    }

    public void gajian() {
        salaryCounter++;
        if (salaryCounter == MIN_SALARY_RECEIVED_BEFORE_RAISE) {
            this.setSalaryCounter(0);
            raiseSalary();
        }
    }

    public void raiseSalary() {
        int newSalary = (salary / 10) + salary;
        System.out.println(name + " mengalami kenaikan gaji sebesar 10% dari " + salary + " menjadi " + newSalary);
        salary = newSalary;
    }
}