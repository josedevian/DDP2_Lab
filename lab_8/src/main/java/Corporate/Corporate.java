package Corporate;

import java.util.ArrayList;

import Corporate.Employee.*;
import Corporate.Employee.Manager.Manager;
import Corporate.Employee.Staff.Staff;
import Corporate.Employee.Intern.Intern;

public class Corporate {
    private static final int MAXEMPLOYEE = 10000;
    private static ArrayList<Employee> employer = new ArrayList<Employee>();
    private static int maxSalary;

    public static Employee find(String name) {
        for (Employee emp : employer) {
            if (name.equals(emp.getName())) {
                return emp;
            }
        }
        return null;
    }

    public static void add(String position, String name, int salary) {
        Employee employee = find(name);
        if (employer.size() < MAXEMPLOYEE) {
            if (!check(employee)) {
                if (position.equals("MANAGER")) {
                    employer.add(new Manager(name, salary));
                }
                if (position.equals("STAFF")) {
                    employer.add(new Staff(name, salary));
                }
                if (position.equals("INTERN")) {
                    employer.add(new Intern(name, salary));
                }
                System.out.println(name + " mulai bekerja sebagai " + position + " di PT TAMPAN");
            } else {
                System.out.println("Karyawan dengan nama " + name + " telah terdaftar");
            }
        } else {
            System.out.println("Jumlah karyawan PT TAMPAN sudah penuh..");
        }
    }

    public static void status(String name) {
        Employee employee = find(name);
        if (!check(employee)) {
            System.out.println(employee.getName() + " " + employee.getSalary());
        } else {
            System.out.println("Karyawan tidak terdaftar");
        }
    }

    public static void addBawahan(String senior, String bawahan) {
        Employee saya = find(senior);
        Employee dia = find(bawahan);
        if (check(saya) && check(dia)) {
            if (saya.getBawahan().size() < saya.getMaxBawahan() && saya.canRecruit(dia)) {
                if (saya.getBawahan().contains(dia)) {
                    System.out.println("Karyawan " + bawahan + " telah menjadi bawahan " + senior);
                } else {
                    saya.addBawahan(dia);
                    System.out.println("Karyawan " + bawahan + " telah berhasil ditambahkan menjadi bawahan " + senior);
                }
            } else {
                if (saya.getBawahan().size() > saya.getMaxBawahan()) {
                    System.out.println("Jumlah bawahan Anda sudah melebihi batas maksimal");
                } else if (!saya.canRecruit(dia)) {
                    System.out.println("Anda tidak layak memiliki bawahan");
                }
            }
        } else {
            System.out.println("Nama tidak berhasil ditemukan");
        }
    }

    public static void gajian() {
        if (employer.size() == 0) {
            System.out.println("Belum ada karyawan");
        } else {
            String cetak = "";
            int count = 0;
            for (Employee emp : employer) {
                emp.gajian();
                if (emp instanceof Staff && emp.getSalary() > maxSalary) {
                    Manager managerBaru = new Manager(emp.getName(), emp.getSalary());
                    managerBaru.setBawahan(emp.getBawahan());
                    employer.set(employer.indexOf(emp), managerBaru);
                    cetak += "Selamat, " + emp.getName() + " telah dipromosikan menjadi MANAGER";
                    count++;
                }
            }
            System.out.println("Semua karyawan telah diberikan gaji");
            if (count != 0) {
                System.out.println(cetak);
            }
        }
    }

    public static boolean check(Employee emp) {
        if (emp == null)
            return false;
        else
            return true;
    }

    public static int getMaxSalary() {
        return maxSalary;
    }

    public static void setMaxSalary(int maxSalary) {
        Corporate.maxSalary = maxSalary;
    }


}