package character;

//  write Monster Class here

public class Monster extends Player{
    private String roar;

    public Monster(String name, int hp, String roar) {
        super(name, hp*2);
        this.roar = roar;
    }

    public Monster(String name, int hp){
        this(name, hp, "AAAAAAaaaAAAAAaaaAAAAAA");
    }

    public String roar(){
        return roar;
    }
}