import character.*;

import java.lang.StringBuffer;
import java.sql.Array;
import java.util.ArrayList;

public class Game {
    ArrayList<Player> player = new ArrayList<Player>();
    ArrayList<Player> termakan = new ArrayList<>();


    /**
     * Fungsi untuk mencari karakter
     *
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name) {
        for (Player pemain : player) {
            if (pemain.getName().equals(name)) {
                return pemain;
            }
        }
        return null;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     *
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int    hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp) {
        if (find(chara) == null) {
            if (tipe.equals("Human")) {
                player.add(new Human(chara, hp));
            }
            if (tipe.equals("Magician")) {
                player.add(new Magician(chara, hp));
            }
            if (tipe.equals("Monster")) {
                player.add(new Monster(chara, hp));
            }
            return chara + " ditambah ke game";
        } else {
            return "Sudah ada karakter bernama " + chara;
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     *
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int    hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar) {
        if (find(chara) == null) {
            player.add(new Monster(chara, hp, roar));
            return chara + " ditambah ke game";
        } else {
            return "Sudah ada karakter bernama " + chara;
        }
    }

    /**
     * fungsi untuk menghapus character dari game
     *
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara) {
        Player pemain = find(chara);
        if (pemain != null) {
            player.remove(pemain);
            return pemain.getName() + " dihapus dari game";
        } else {
            return "Tidak ada " + chara;
        }
    }


    /**
     * fungsi untuk menampilkan status character dari game
     *
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara) {
        Player character = find(chara);
        String hasil = "";
        if (character != null) {
            hasil += (character.getJenis() + " " + character.getName() + "\n" + "HP: " + character.getHp() + "\n");
            if (character.getHp() == 0) {
                hasil += "Sudah meninggal dunia dengan damai\n";
            } else {
                hasil += "Masih hidup\n";
            }
            hasil += diet(chara);
        }
        return hasil;
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     *
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status() {
        String hasil = "";
        if (player.size() > 0) {
            hasil += "\n";
            for (Player character : player) {
                hasil += status(character.getName());
            }
        } else {
            hasil += "Tidak ada pemain";
        }
        return hasil;
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     *
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public StringBuffer diet(String chara) {
        Player character = find(chara);
        StringBuffer hasil = new StringBuffer();
        if (character != null) {
            if (character.getListDimakan().size() != 0) {
                hasil.append("Memakan");
                for (Player termakan : character.getListDimakan()) {
                    hasil.append(termakan.getJenis()).append(" ").append(termakan.getName()).append(", ");
                }
                hasil.delete(hasil.length() - 2, hasil.length());
                hasil.append("\n");
            } else {
                hasil.append("Belum memakan siapa-siapa\n");
            }
        } else {
            hasil.append("Tidak ada ").append(chara);
        }
        return hasil;
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     *
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet() {
        String hasil = "";
        if (termakan.size() > 0) {
            hasil += "Termakan: ";
            for (Player chara : termakan) {
                hasil += (chara.getJenis() + " " + chara.getName() + ", ");
            }
        } else {
            hasil += "Belum ada yang termakan";
        }
        return hasil;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName) {
        Player saya = find(meName);
        Player musuh = find(enemyName);
        if (cekPemain(saya) && cekPemain(musuh)) {
            if (saya.getHp() != 0) {
                saya.attack(musuh);
                return "Nyawa " + musuh.getName() + " " + musuh.getHp();
            } else {
                return saya.getName() + " tidak bisa menyerang " + musuh.getName();
            }
        } else {
            return "Tidak ada " + saya.getName() + " atau " + musuh.getName();
        }

    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName) {
        Player saya = find(meName);
        Player musuh = find(enemyName);
        String hasil = "";
        if (!cekPemain(saya) || !cekPemain(musuh)) {
            hasil += "Tidak ada" + meName + " atau " + enemyName;
        } else if (saya instanceof Magician && saya.getHp() > 0) {
            ((Magician) saya).burn(musuh);
            hasil += "Nyawa " + musuh.getName() + " " + musuh.getHp();
            if (musuh.getHp() == 0) {
                hasil += "\n dan matang";
            }
        } else {
            hasil = meName + " tidak bisa membakar " + enemyName;
        }
        return hasil;
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     *
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName) {
        Player saya = find(meName);
        Player musuh = find(enemyName);
        String hasil = "";
        if (!cekPemain(saya) || !cekPemain(musuh)) {
            hasil += "Tidak ada" + meName + " atau " + enemyName;
        } else if (saya.canEat(musuh) && saya.getHp() > 0) {
            saya.eat(musuh);
            player.remove(musuh);
            termakan.add(musuh);
            hasil += meName + " memakan " + enemyName + "\nNyawa " + meName + " kini " + saya.getHp();
        }
        return hasil;

    }

    /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     *
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName) {
        Player saya = find(meName);
        if (!cekPemain(saya)) {
            return "Tidak ada " + meName;
        } else if (saya instanceof Monster && saya.getHp() > 0) {
            return ((Monster) saya).roar();
        } else {
            return meName + " tidak bisa berteriak";
        }
    }

    /**
     * fungsi tambahan untuk mengecek apakah nama pemain ada di list pemain
     *
     * @param pemain untuk nama pemain yang akan dicek
     * @return true jika ada, false jika tidak ada
     */
    public boolean cekPemain(Player pemain) {
        if (pemain == null) {
            return false;
        } else {
            return true;
        }
    }
}
