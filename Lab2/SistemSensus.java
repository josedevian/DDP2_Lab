import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Jose Devian Hibono, NPM 1706039603, Kelas B, GitLab Account: gitlab.com/josedevian
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		String nama = input.nextLine();
		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();
		System.out.print("Panjang Tubuh (cm)     : ");
		String panjang = input.nextLine();
		int panjangInt = Integer.parseInt(panjang);
		System.out.print("Lebar Tubuh (cm)       : ");
		String lebar = input.nextLine();
		int lebarInt = Integer.parseInt(lebar);
		System.out.print("Tinggi Tubuh (cm)      : ");
		String tinggi = input.nextLine();
		int tinggiInt = Integer.parseInt(tinggi);
		System.out.print("Berat Tubuh (kg)       : ");
		String berat = input.nextLine();
		float beratFloat = Float.parseFloat(berat);
		System.out.print("Jumlah Anggota Keluarga: ");
		String makanan =input.nextLine();
		int makananInt = Integer.parseInt(makanan);
		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();
		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();
		System.out.print("Jumlah Cetakan Data    : ");
		String jumlahCetakan = input.nextLine();
		int jumlahCetakanInt = Integer.parseInt(jumlahCetakan);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		float rasio = beratFloat / ((float) panjangInt/100) * ((float)lebarInt/100) * ((float)tinggiInt/100);
		int rasioInt = (int)rasio;

		for (int angka = 1; angka <= jumlahCetakanInt; angka++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + angka + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase
			System.out.print("DATA SIAP DICETAK UNTUK " + penerima + "\n--------------------\n" );
			System.out.println(nama + " - " + alamat);
			System.out.println("Lahir pada tanggal " + tanggalLahir);
			System.out.println("Rasio Berat per Volume     = " + rasioInt + " kg/m^3");

			// TODO Periksa ada catatan atau tidak
			if (catatan.isEmpty()) catatan = "Tidak ada catatan.";
			else catatan = catatan;

			System.out.println("Catatan: " + catatan);

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			//String hasil = "";
			//.....;
		}

		
		// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)


		/*
		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		for(int a = 0; a < nama.length(); a++)

		String nomorKeluarga = ((panjangInt * lebarInt * lebarInt) + (int)nama)

		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		//... anggaran = (...) (...);

		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		... tahunLahir = .....; // lihat hint jika bingung
		... umur = (...) (...);

		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)





		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi = "";
		.....;*/

		input.close();
	}
}
