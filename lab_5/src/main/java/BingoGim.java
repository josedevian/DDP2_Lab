import java.util.Scanner;

public class BingoGim{
	public static void main(String[] args){
		System.out.println("Selamat Datang di GIM BINGO\nSilakan masukan angka untuk kartu anda.\nSelamat Bermain!");
		boolean play = (true);
		Scanner input = new Scanner(System.in);
		Number[][] angkaBingo = new Number[5][5];
		Number[] states = new Number[100];
		for(int i = 0; i < 5; i++){
			for(int j = 0; j < 5; j++){					//membuat loop untuk mengambil angka yang akan dimasukan ke dalam array
				int nilai = input.nextInt();
				angkaBingo[i][j] = new Number(nilai, i, j);
				states[nilai] = new Number(nilai, i, j);
			}
		}
		BingoCard kartu = new BingoCard(angkaBingo, states);
		//membuat input untuk user
		while(play){
			String command = input.next();
			if(command.equals("MARK")){
				int num = input.nextInt();
				System.out.println(kartu.markNum(num));
			}
			else if(command.equals("INFO")){
				System.out.println(kartu.info());
			}
			else if(command.equals("RESTART")){
				kartu.restart();
			}
			else{
				System.out.println("Maaf, perintah yang anda masukan salah. Silakan coba lagi.");
			}
			if(kartu.isBingo()){
				System.out.println("BINGO!!");
				System.out.println(kartu.info());
				System.out.println("Anda sudah menang, terima kasih sudah bermain.");
				break;
			}
		}
		
	}
}

//Jose Devian Hibono | 1706039603 | DDP-2 B/NN