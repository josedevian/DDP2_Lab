import java.util.Scanner;

public class Manusia{ 					//nama kelas

	private String nama;				//encapsulation dan declaring Instance variable, menggunakan access modifier private
	private int umur;
	private int uang;
	private float kebahagiaan = 50;

	public Manusia(String nama, int umur, int uang){	//membuat constructor kelas Manusia
		this.nama = nama;
		this.umur = umur;
		this.uang = uang;
	}
	//Setters dan Getters
	public Manusia(String nama, int umur){ 
		this(nama,umur,50000);
	}
	public void setNama(String nama){
		this.nama = nama;		
	}
	public String getNama(){
		return this.nama;
	}
	public void setUmur(int umur){
		this.umur = umur;
	}
	public int getUmur(){
		return this.umur;
	}
	public void setUang(int uang){
		this.uang = uang;
	}
	public int getUang(){
		return this.uang;
	}
	public void setKebahagiaan(float kebahagiaan){
		this.kebahagiaan = kebahagiaan;
	}
	public float getKebahagiaan(){
		return this.kebahagiaan;
	}
	//membuat method beriUang
	public void beriUang(Manusia penerima){
		int panjang = penerima.nama.length();
		int totalnilai = 0;
		for(int a = 0; a < panjang; a++){			//mengkonversikan setiap huruf dari nama penerima 
			char huruf = penerima.nama.charAt(a);	//menjadi jumlah nilai ASCII tiap karakter
			int kata = (int)huruf;
			totalnilai = totalnilai + kata;
		}
		int jumlah = totalnilai*100;
		if(this.uang > jumlah){						//jumlah uang penerima akan bertambah
			penerima.uang += jumlah;				//jumlah uang pengirim akan berkurang
			this.uang -= jumlah;
			float jumlahFloat = jumlah;

			penerima.kebahagiaan += (jumlahFloat/6000);
			penerima.kebahagiaan = Math.min(penerima.kebahagiaan, 100); //agar jumlah kebahagiaan tidak melebihi 100 (maks)
			this.kebahagiaan += (jumlahFloat/6000);
			this.kebahagiaan = Math.min(this.kebahagiaan, 100);			//agar jumlah kebahagiaan tidak melebihi 100 (maks)
			
			System.out.println(this.nama + " memberi uang sebanyak " + jumlah + " kepada " + penerima.nama + ", mereka berdua senang.");
		}
		else{
			System.out.println(this.nama +" ingin memberi uang kepada " + penerima.nama +" namun tidak memiliki cukup uang :'(");
		}
	}
	//membuat method beriUang dengan parameter lebih spesifik (ada jumlah uang yang akan dikirim lebih spesifik)
	public void beriUang(Manusia penerima, int jumlah){
		if(this.uang > jumlah){
			penerima.uang += jumlah;
			this.uang -= jumlah;
			float jumlahFloat = jumlah;

			penerima.kebahagiaan += (jumlahFloat/6000);
			penerima.kebahagiaan = Math.min(penerima.kebahagiaan, 100); //agar jumlah kebahagiaan tidak melebihi 100 (maks)
			this.kebahagiaan += (jumlahFloat/6000);
			this.kebahagiaan = Math.min(this.kebahagiaan, 100);			//agar jumlah kebahagiaan tidak melebihi 100 (maks)
			
			System.out.println(this.nama + " memberi uang sebanyak " + jumlah +" kepada " + penerima.nama + ", mereka berdua senang.");
		}
		else{
			System.out.println(this.nama + " ingin memberi uang kepada " + penerima.nama + " namun tidak memiliki cukup uang :'(");
		}
	}
	//membuat method bekerja
	public void bekerja(int durasi, int bebanKerja){
		if(this.umur < 18){
			System.out.println(this.nama + " belum boleh bekerja karena masih di bawah umur D:");
		}
		int pendapatan = 0;
		int bebanKerjaTotal = bebanKerja * durasi;
		if(bebanKerjaTotal <= this.kebahagiaan){
			this.kebahagiaan -= bebanKerjaTotal;
			pendapatan = bebanKerjaTotal * 10000;
			System.out.println(this.nama + " bekerja FULL TIME, total pendapatan " + pendapatan);
		}
		else if(bebanKerjaTotal > this.kebahagiaan){
			float durasiBaruFloat = this.kebahagiaan / bebanKerja;
			int durasiBaru = (int)durasiBaruFloat;
			bebanKerjaTotal = durasiBaru * bebanKerja;
			pendapatan = bebanKerjaTotal * 10000;
			this.kebahagiaan -= bebanKerjaTotal;
			System.out.println(this.nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);	
		}
		this.uang += pendapatan;
	}
	//membuat method rekreasi
	public void rekreasi(String namaTempat){
		int biaya = 0;
		int panjangTempat = namaTempat.length();
		biaya += (panjangTempat * 10000);

		if(this.uang > biaya){
			this.uang -=biaya;
			this.kebahagiaan += panjangTempat;
			this.kebahagiaan = Math.min(this.kebahagiaan, 100);	//agar jumlah kebahagiaan tidak melebihi 100 (maks)
			System.out.println(this.nama + " berekreasi di " + namaTempat + ", " + this.nama + " senang:)");
		}
		else{
			System.out.println(this.nama + " tidak mempunyai cukup uang untuk berekreasi di " + namaTempat);
		}
	}
	//membuat method sakit
	public void sakit(String namaPenyakit){
		int panjangPenyakit = namaPenyakit.length();
		this.kebahagiaan -= panjangPenyakit;
		this.kebahagiaan = Math.max(0, this.kebahagiaan);		//agar jumlah kebahagiaan tidak kurang dari 0 (minimum)
		System.out.println(this.nama + " terkena penyakit " + namaPenyakit + " :O");
	}
	//membuat method toString untuk mencetak representasi dari sebuah Objek
	public String toString(){
		return String.format("Nama\t\t: " + this.nama + "\nUmur\t\t: " + this.umur + "\nUang\t\t: " + this.uang + "\nKebahagiaan\t: " + this.kebahagiaan);
	}
}

//Jose Devian Hibono, 1706039603, DDP2-B-NN