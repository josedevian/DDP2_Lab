package theater;
import ticket.Ticket;
import movie.Movie;
import java.util.ArrayList;

public class Theater{
	private String namaBioskop;
	private int saldo;
	private ArrayList isiTicket;
	private Movie[] movie;

	public Theater (String namaBioskop, int saldo, ArrayList isiTicket, Movie[] movie){
		this.namaBioskop = namaBioskop;
		this.saldo = saldo;
		this.isiTicket = isiTicket;
		this.movie = movie;
	}
	public String getNamaBioskop(){
		return namaBioskop;
	}
	public void setNamaBioskop(String bioskop){
		this.namaBioskop = namaBioskop;
	}
	public int getSaldo(){
		return saldo;
	}
	public void setSaldo(int saldo){
		this.saldo = saldo;
	}
	public ArrayList<Ticket> getIsiTicket(){
		return isiTicket;
	}
	public void setIsiTicket(ArrayList<Ticket> isiTicket){
		this.isiTicket = isiTicket;
	}
	public Movie[] getMovie(){
		return movie;
	}
	public void setMovie(Movie[] movie){
		this.movie = movie;
	}
	// membuat method info film
	public void printInfo(){
		System.out.printf("------------------------------------------------------------------\nBioskop\t\t\t: %s\nSaldo Kas\t\t: %d\nJumlah tiket tersedia\t: %d\n", namaBioskop, saldo, isiTicket.size());
		System.out.print("Daftar Film tersedia\t: ");
		int jumlahFilm = movie.length;
		for(int i=0; i<jumlahFilm; i++){
			if(i == jumlahFilm - 1){
				System.out.print(movie[i].getJudul());
			}
			else{
				System.out.print(movie[i].getJudul() + ", ");
			}
		}
		System.out.println("\n------------------------------------------------------------------");
	}
	// membuat method info pendapatan
	public static void printTotalRevenueEarned(Theater[] listTheaters){
		int jumlahPendapatan = 0;
		for(Theater bioskop: listTheaters){
			jumlahPendapatan = jumlahPendapatan + bioskop.getSaldo();
		}
		System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + jumlahPendapatan);
		System.out.println("------------------------------------------------------------------");
		for (Theater bioskop: listTheaters) {
            System.out.println("Bioskop\t\t: " + bioskop.getNamaBioskop());
            System.out.println("Saldo Kas\t: Rp." + bioskop.getSaldo() + "\n");
        }
        System.out.println("------------------------------------------------------------------");
	}
}

// Jose Devian Hibono | 1706039603 | DDP-2-B | NN |