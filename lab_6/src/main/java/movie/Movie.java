package movie;

public class Movie{
	private String judul;
	private String genre;
	private int durasi;
	private String rating;
	private String jenisFilm;
	private int umurMinimal;

	public Movie(String judul, String genre, int durasi, String rating, String jenisFilm){
		this.judul = judul;
		this.genre = genre;
		this.durasi = durasi;
		this.rating = rating;
		this.jenisFilm = jenisFilm;
	}
	public String getJudul(){
		return judul;
	}
	public String getGenre(){
		return genre;
	}
	public int getDurasi(){
		return durasi;
	}
	public String getRating(){
		return rating;
	}
	public String getJenisFilm(){
		return jenisFilm;
	}
	public int getUmurMinimal(){
		return umurMinimal;
	}
	public int MinUmur(){
		switch(this.genre){
			case "Dewasa":
				return 17;
			case "Remaja":
				return 13;
			default:
				return 0;
		}
	}
	public String movies(){
		return ("------------------------------------------------------------------\nJudul\t: " + this.judul + "\nGenre\t: " + this.genre + "\nDurasi\t: " + this.durasi + "\nRating\t: " + this.jenisFilm + "\n------------------------------------------------------------------");
	}
}

// Jose Devian Hibono | 1706039603 | DDP-2-B | NN |