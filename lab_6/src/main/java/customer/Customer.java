package customer;
import ticket.Ticket;
import movie.Movie;
import theater.Theater;

public class Customer{
	private String nama;
	private int umur;
	private String isFemale;
	private Ticket hasTicket;

	public Customer(String nama, String isFemale, int umur){
		this.nama = nama;
		this.isFemale = isFemale;
		this.umur = umur;
	}
	// membuat method findMovie
	public void findMovie(Theater bioskop, String namaFilm){
		int count = 0;
		for(Movie film : bioskop.getMovie()){
			if(film.getJudul().equals(namaFilm)){
				System.out.println("\n------------------------------------------------------------------");
                System.out.println("Judul\t: " + film.getJudul() + "\nGenre\t: " + film.getRating());
                System.out.println("Durasi\t: " + film.getDurasi() +" menit" + "\nRating\t: " + film.getGenre());
                System.out.println("Jenis\t: Film " + film.getJenisFilm());
                System.out.println("\n------------------------------------------------------------------");
                break;
			}
			else{
				count++;
			}
		}
		if(count == bioskop.getMovie().length){
			System.out.println("Film " + namaFilm + " yang dicari " + this.nama + " tidak ada di bioskop " + bioskop.getNamaBioskop());
		}
	}
	// membuat method orderTicket
	public Ticket orderTicket(Theater bioskop, String namaFilm, String hari, String jenisFilm){
		int count = 0;
		Ticket beli = null;
		for(Ticket tiket : bioskop.getIsiTicket()){
			String judulTemp = tiket.getFilm().getJudul();
			String hariTemp = tiket.getHari();
			String jenisTemp = tiket.isIs3D();
			if(judulTemp.equals(namaFilm) && hariTemp.equals(hari) && jenisTemp.equals(jenisFilm)){
				if(this.umur >= tiket.getFilm().MinUmur()){
					System.out.printf("%s telah membeli tiket %s jenis %s di %s pada hari %s seharga Rp. %d\n", this.nama, judulTemp, jenisTemp, bioskop.getNamaBioskop(), hariTemp, tiket.getHarga());
					bioskop.setSaldo(bioskop.getSaldo() + tiket.getHarga());
					beli = tiket;
					break;
				}
				else{
					System.out.printf("%s masih belum cukup umur untuk menonton %s dengan rating %s\n", this.nama, tiket.getFilm().getJudul(), tiket.getFilm().getGenre());
					break;
				}
			}
			else{
				count++;
			}
		}
		if(count == bioskop.getIsiTicket().size()){
			System.out.printf("Tiket untuk film %s jenis %s dengan jadwal %s tidak tersedia di %s\n", namaFilm, jenisFilm, hari, bioskop.getNamaBioskop());
		}
		return beli;
	}
}

// Jose Devian Hibono | 1706039603 | DDP-2-B | NN |