package ticket;
import movie.Movie;

public class Ticket{
	private Movie film;
	private String hari;
	private int harga = 60000;
	private boolean is3D;

	public Ticket(Movie film, String hari, boolean is3D){
		this.film = film;
		this.hari = hari;
		this.is3D = is3D;
		if(hari.equals("Sabtu")||hari.equals("Minggu")){
			this.harga += 40000;
		}
		if(is3D){
			this.harga = harga + (int)(harga*0.2);
		}
	}
	public int getHarga(){
		return harga;
	}
	public Movie getFilm(){
		return film;
	}
	public String getHari(){
		return hari;	
	}
	public String isIs3D(){
		if(is3D){
			return "3 Dimensi";
		}
		else{
			return "Biasa";
		}
		
	}
}

// Jose Devian Hibono | 1706039603 | DDP-2-B | NN |
