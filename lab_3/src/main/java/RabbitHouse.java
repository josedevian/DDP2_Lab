import java.util.Scanner;

public class RabbitHouse{

	// membuat fungsi rekursif yang diminta
	public static int hitung(int panjang, int jumlah){
		if(panjang == 1){
			return 1;
		}
		else{
			return (panjang * jumlah + hitung(panjang-1, panjang * jumlah)); //menghitung jumlah kelinci yang ada dengan menghitung semua keturunan kelinci
		}
	}


	public static void main (String[] args){
		// membuat input Scanner baru
		Scanner input = new Scanner(System.in);

		// meminta masukan kepada user dan menghitung panjang nama
		String mode  = input.next();
		String nama = input.next();
		int panjang = nama.length();

		// menentukan mode yang digunakan
		if(mode.equals("normal")){
			System.out.println(hitung(panjang, 1));
		}
		else if(mode.equals("palindrom")){
			System.out.println("Maaf, mode yang anda masukan saat ini belum tersedia.");
		}
		else{
			System.out.println("Maaf, perintah ini tidak tersedia. Mohon coba lagi.");
		}
		input.close();
	}
}

//Jose Devian Hibono, NPM 1706039603