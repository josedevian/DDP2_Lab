package lab9;

import lab9.user.User;
import lab9.event.Event;

import java.util.ArrayList;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.math.BigInteger;

/**
 * Class representing event managing system
 */
public class EventSystem {
    /**
     * List of events
     */
    private ArrayList<Event> events;

    /**
     * List of users
     */
    private ArrayList<User> users;

    /**
     * Constructor. Initializes events and users with empty lists.
     */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * addEvent method, for adding event to the events list
     *
     * @param name
     * @param startTimeStr
     * @param endTimeStr
     * @param costPerHourStr
     * @return adding the event to the list if the event meets the terms specified.
     * @return if the event doesn't meet the specified requirements, then it will tell the user which term is not qualified
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        Event event = findEvent(name);
        if (event == null) {
            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss");
            LocalDateTime mulai = LocalDateTime.parse(startTimeStr, format);
            LocalDateTime kelar = LocalDateTime.parse(endTimeStr, format);
            if (kelar.isAfter(mulai)) {
                events.add(new Event(name, mulai, kelar, new BigInteger(costPerHourStr)));
                return "Event " + name + " berhasil ditambahkan!";
            }
            return "Waktu yang diinputkan tidak valid!";
        }
        return "Event " + name + " sudah ada!";
    }

    /**
     * addUser method, for adding user to the users list
     *
     * @param name
     * @return statement
     */
    public String addUser(String name) {
        User user = findUser(name);
        if (user == null) {
            users.add(new User(name));
            return "User " + name + " berhasil ditambahkan!";
        } else {
            return "User " + name + " sudah ada!";
        }
    }

    /**
     * registerToEvent method, for users to register to the event
     * @param userName, the name of the user
     * @param eventName, the name of the event
     * @return
     */
    public String registerToEvent(String userName, String eventName) {
        User user = findUser(userName);
        Event event = findEvent(eventName);
        if (user == null && event == null) {
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        } else if (user == null) {
            return "Tidak ada pengguna dengan nama " + userName + "!";
        } else if (event == null) {
            return "Tidak ada acara dengan nama " + eventName + "!";
        } else if (!user.addEvent(event)) {
            return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
        } else {
            return userName + " berencana menghadiri " + eventName + "!";
        }

    }

    /**
     * findUser method, for checking whether a certain user (namaUser) is in that ArrayList
     *
     * @param namaUser, the name of the user
     * @return that user if found in the list, null if not
     */
    public User findUser(String namaUser) {
        for (User user : users) {
            if (namaUser.equals(user.getName())) {
                return user;
            }
        }
        return null;
    }

    /**
     * findEvent method, for checking whether a certain event name is already in the list or not
     *
     * @param namaEvent, the name of the event
     * @return that event if found in the list, null if not
     */
    public Event findEvent(String namaEvent) {
        for (Event event : events) {
            if (namaEvent.equals(event.getName())) {
                return event;
            }
        }
        return null;
    }

    /**
     * getUser method, getting the user object
     * @param name
     * @return user object
     */
    public User getUser(String name) {
        return findUser(name);
    }

    /**
     * getEvent method, getting the event object
     * @param eventName
     * @return event object
     */
    public Event getEvent(String eventName){
        return findEvent(eventName);
    }
}