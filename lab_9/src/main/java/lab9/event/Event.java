package lab9.event;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.math.BigInteger;
import java.time.temporal.ChronoUnit;


/**
 * A class representing an event and its properties
 */
public class Event implements Comparable<Event> {
    /**
     * Name of event
     */
    private String name;

    // TODO: Make instance variables for representing beginning and end time of event
    private LocalDateTime waktuMulai;
    private LocalDateTime waktuKelar;

    // TODO: Make instance variable for cost per hour
    private BigInteger cost;

    // TODO: Create constructor for Event class

    /**
     * Constructor for the Event
     * Initializes an event object
     *
     * @param name
     * @param waktuMulai
     * @param waktuKelar
     * @param cost
     */
    public Event(String name, LocalDateTime waktuMulai, LocalDateTime waktuKelar, BigInteger cost) {
        this.name = name;
        this.waktuMulai = waktuMulai;
        this.waktuKelar = waktuKelar;
        this.cost = cost;
    }

    /**
     * Accessor for name field.
     *
     * @return name of this event instance
     */
    public String getName() {
        return this.name;
    }

    public BigInteger getCost() {
        return cost;
    }
    // TODO: Implement toString()

    /**
     * toString method for printing an event descriptions such as event's name, start and end time, and the event's estimated cost
     *
     * @return event's name, start and end time, and the event's estimated cost
     */
    @Override
    public String toString() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy, HH:mm:ss");
        return name + "\nWaktu mulai: " + waktuMulai.format(format) + "\nWaktu selesai: " + waktuKelar.format(format) + "\nBiaya kehadiran: " + cost;
    }

    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.

    /**
     * overlapsWith method to test if this event overlaps with another event
     *
     * @param otherEvent
     * @return true if it does
     * @return false if it doesn't
     */
    public boolean overlapsWith(Event otherEvent) {
        return this.waktuKelar.isBefore(otherEvent.waktuMulai) || otherEvent.waktuKelar.isBefore(this.waktuMulai);
    }

    /**
     * compareTo method, for comparing time between this event's start time and other event's start time
     * @param other
     * @return amount of time between both events
     */
    @Override
    public int compareTo(Event other) {
        return (int)(other.waktuMulai.until(this.waktuMulai, ChronoUnit.SECONDS));
    }

}
