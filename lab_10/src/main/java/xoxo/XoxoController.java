package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;
import xoxo.util.XoxoMessage;
import xoxo.key.HugKey;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoController {

    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;
    private static int countFileEnc = 0; //counter of the encrypted file
    private static int countFileDec = 0; //counter of the decrypted file
    public static final int DEFAULT_SEED = 18;
    private static String OS = System.getProperty("os.name").toLowerCase();

    /**
     * Class constructor given the GUI object.
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        gui.setEncryptFunction(e -> encrypt());
        gui.setDecryptFunction(e -> decrypt());
    }

    public void encrypt(){
        String messageText = gui.getMessageText();
        String kissKey = gui.getKeyText();
        String seed = gui.getSeedText();
        XoxoEncryption encryptMsg = null;
        try {
            encryptMsg = new XoxoEncryption(kissKey);
        } catch (KeyTooLongException e) {
            gui.setWarning("Key length must not exceed 28");
            return;
        } catch (InvalidCharacterException e) {
            gui.setWarning("Key can contain only A-Z, a-z, and @");
            return;
        }
        XoxoMessage message;
        try {
            if (seed.equals("") || Integer.parseInt(seed) == 18 || seed.equals("DEFAULT_SEED")) {
                message = encryptMsg.encrypt(messageText);
                seed = "DEFAULT_SEED";
            } else {
                message = encryptMsg.encrypt(messageText, Integer.parseInt(seed));
            }
        } catch (RangeExceededException e) {
            gui.setWarning("Seed must between 0 and 36 (inclusive)");
            return;
        } catch (NumberFormatException e) {
            gui.setWarning("Seed must be a number!");
            return;
        } catch (SizeTooBigException e) {
            gui.setWarning("Message size too big (more than 10Kbit)!");
            return;
        }
        encryptedFileWrite(message, seed);
    }

    public void decrypt(){
        String hugKey = gui.getKeyText();
        String encryptedMessage = gui.getMessageText();
        XoxoDecryption decryptMsg = new XoxoDecryption(hugKey);
        String seedStr = gui.getSeedText();
        int seed;
        try {
            if (seedStr.equals("") || Integer.parseInt(seedStr) == 18 || seedStr.equals("DEFAULT_SEED")) {
                seed = DEFAULT_SEED;
            } else {
                seed = Integer.parseInt(seedStr);
            }
        } catch (NumberFormatException e) {
            seed = DEFAULT_SEED;
            gui.setWarning("Seed must be a number!");
        }
        String decryptedMessage = decryptMsg.decrypt(encryptedMessage, seed);
        decryptedFileWrite(decryptedMessage);
    }

    public void decryptedFileWrite(String decryptMessage){
        String filename = System.getProperty("user.dir") + "/DecryptResult/fileDecrypted" + countFileDec + ".txt";
        if (OS.contains("win")) {
            filename = filename.replace('/', '\\');
        }
        File file = new File(filename);
        boolean isCreated = false;
        try {
            isCreated = file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(decryptMessage);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            gui.appendLog("Decrypt failed! Cannot create file.");
            return;
        }
        if (isCreated) {
            gui.appendLog("Decrypted text: " + decryptMessage);
            countFileDec++;
        } else {
            gui.appendLog("Text failed to decrypt.");
        }

    }

    public void encryptedFileWrite(XoxoMessage encryptMessage, String seed) {
        String filename = System.getProperty("user.dir") + "/EncryptResult/fileEncrypted" + countFileEnc + ".enc";
        if (OS.contains("win")) {
            filename = filename.replace('/', '\\');
        }
        File file = new File(filename);
        boolean isCreated = false;
        try {
            isCreated = file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(encryptMessage.getEncryptedMessage());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            gui.appendLog("Encrypt failed! Cannot create file.");
            return;
        }
        if (isCreated) {
            gui.appendLog("Encrypted text: " + encryptMessage.getEncryptedMessage());
            gui.appendLog("Hug key: " + encryptMessage.getHugKey().getKeyString());
            gui.appendLog("Seed: " + seed);
            countFileEnc++;
        } else {
            gui.appendLog("Text failed to encrypt.");
        }
    }


    //TODO: Create any methods that you want
}