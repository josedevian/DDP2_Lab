package xoxo;

import java.awt.event.ActionListener;
import java.awt.*;
import javax.swing.*;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoView {
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    private JFrame frame;
    private JPanel topPanel;
    private JPanel bottomPanel;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        frame = new JFrame("Xoxo Encryptor / Decryptor");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        topPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        bottomPanel = new JPanel(new GridLayout(1, 1));
        JPanel textPanel = new JPanel(new GridLayout(3,1));
        JPanel messagePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel keyPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel seedPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JPanel encryptPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel decryptPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel buttonPanel = new JPanel(new GridLayout(2, 1));
        JLabel messageLabel = new JLabel("Message    :");
        JLabel keyLabel = new JLabel("Key            :");
        JLabel seedLabel = new JLabel("Seed          :");
        messageField = new JTextField();
        messageField.setPreferredSize(new Dimension(200,25));
        keyField = new JTextField();
        keyField.setPreferredSize(new Dimension(200,25));
        seedField = new JTextField();
        seedField.setPreferredSize(new Dimension(200,25));
        encryptButton = new JButton("Encrypt");
        encryptButton.setPreferredSize(new Dimension(90, 30));
        decryptButton = new JButton("Decrypt");
        decryptButton.setPreferredSize(new Dimension(90, 30));
        logField = new JTextArea("=== Log ===\n");
        logField.setPreferredSize(new Dimension(400,300));

        messagePanel.add(messageLabel);
        messagePanel.add(messageField);
        keyPanel.add(keyLabel);
        keyPanel.add(keyField);
        seedPanel.add(seedLabel);
        seedPanel.add(seedField);

        textPanel.add(messagePanel);
        textPanel.add(keyPanel);
        textPanel.add(seedPanel);

        encryptPanel.add(encryptButton);
        decryptPanel.add(decryptButton);

        buttonPanel.add(encryptPanel);
        buttonPanel.add(decryptPanel);

        topPanel.add(textPanel);
        topPanel.add(buttonPanel);
        bottomPanel.add(logField);
        frame.add(topPanel, BorderLayout.NORTH);
        frame.add(bottomPanel, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);

    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }

    /**
     * Method to set warning to GUI if it catch exceptions.
     * @param warning the message that is going to pop-out
     */
    public void setWarning(String warning) {
        JOptionPane.showMessageDialog(frame, warning);
    }

}