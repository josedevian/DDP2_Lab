package xoxo.exceptions;

/**
 * <write the documentation>
 */
public class SizeTooBigException extends RuntimeException{

    public SizeTooBigException(String message){
        super(message);
    }
    
}