public class CekUmur{

	public static void main(String[] args){
		int umur = 19;
		String masamasa;

		if(umur<10)
			masamasa = "Anak-anak";
		else if(umur<20)
			masamasa = "Remaja";
		else if(umur<50)
			masamasa = "Dewasa";
		else{
			masamasa = "Lansia";
		}
		System.out.println(masamasa);
	}
}