# **Tutorial 6: Case Study OO (Soal Tutorial)**

Dasar-dasar Pemrograman 2 - CSGE601021 | Fakultas Ilmu Komputer, Universitas Indonesia, Semester Genap 2017/2018

**Dibuat oleh JIA | RF**

* * *
Pada tutorial sebelumnya, Anda telah mempelajari mengenai konsep array di Java. Keren, kalian sudah mempunyai basic untuk mengimplementasikan OO ke depannya! 
Di tutorial kali ini, kalian akan dipaparkan mengenai pengertian *cohesion*, dan *coupling* sebagai salah satu parameter kalian dalam mendesain sebuah kelas yang baik.

## **Tujuan Pembelajaran**
1. Memahami definisi dari *cohesion*
2. Memahami definisi dari *coupling*
3. Memahami mengapa *cohesion* dalam mendesain sebuah kelas itu penting
3. Memahami mengapa *coupling* dalam mendesain sebuah kelas itu penting

### **Before You Start...**
1. Lakukan `git pull upstream master` dengan menggunakan Command Prompt pada folder repository lokal Anda.
2. Kerjakan soal tersebut kemudian letakkan file jawaban anda di folder `lab_6/src/main/java`. Kami sudah menyediakan folder kosong untuk kamu.
3. Setelah selesai mengimplementasikan kode tersebut, lakukan add, commit, dan push code kalian tersebut.

## **Materi Tutorial**

### **What is Cohesion**
*Cohesion* adanya tingkat ke-"fokus"-an sebuah *class*; atau seberapa fokus sebuah *class* merepresentasikan sebuah benda/konsep. Keuntungan dari pengembangan dengan *cohesion* yang tinggi adalah kemudahan bagi kita maupun pengguna untuk memahami *class* tersebut. Selain itu, dalam pengembangan program, kita menginginkan *class-class* untuk tujuan yang spesifik. Agar tercapai *cohesion* yang tinggi, semua method maupun variabel di dalam *class* seharusnya merepresentasikan hanya sebuah objek/konsepp bukan yang lain.

Sebagai contoh, kelas Mobil hanya mengandung keperluan dari sebuah mobil saja. Apabila kita ingin menambahkan fitur untuk membeli mobil, tambahkan fitur tersebut di sebuah **kelas baru yang bertanggung jawab untuk pembelian mobil** bukan di kelas Mobil itu sendiri. Desain program yang baik yakni program yang mempunyai *cohesion* yang tinggi.

#### **Example of Low Cohesion**
-------------------
| Staff           |
-------------------
| checkEmail()    |
| sendEmail()     |
| emailValidate() |
| PrintLetter()   |
-------------------

#### **Example of High Cohesion**
----------------------------
| Staff                   |
----------------------------
| -salary                 |
| -emailAddr              |
----------------------------
| setSalary(newSalary)    |
-
| getSalary()             |
| setEmailAddr(newEmail)  |
| getEmailAddr()          |
----------------------------

Sumber: https://stackoverflow.com/questions/3085285/cohesion-coupling

### **What is Coupling**
Coupling adalah tingkat keberkaitan suatu *class* kepada *class-class* yang lain. *Class* A dianggap berkaitan dengan *class* B jika:
- B disimpan sebagai variabel di A
- B merupakan parameter method di A
- B merupakan return type method di A

Ketika *class* A berkaitan dengan *class* B, ada beberapa konsekuensi:
- Ketika kita membutuhkan *class* A, kita harus mengikutkan *class* B
- Ketika kita memodifikasi *class* B, kita mungkin harus memodifikasi *class* A pula
Desain program yang baik yakni program yang mempunyai tingkat *coupling* yang rendah.

#### **Example of Tight Coupling**
```java
class Names{
	public String name;
	
	public String getName(){
		//some code to check valid access to name
		return name;
	}
	
	public void setName(String s){
		//some code to check valid settingto name
		name=s;
	}
}

class ModifyData{
	public void updateName(){
		Name ob = new Name();
		ob.name="Hello"; //Directly accessing name with dot operator shows tight coupling between two classes
		System.out.println(ob.name); //Tight coupling because of bad encapsulation
	}
}
```

Analisis Program:
- *Class Name* mempunyai *instance variable*, name; yang dideklarasikan **public**
- *Class Name* mempunyai 2 *public method* *getter, setter* yang memvalidasi akses ke variabel name sebelum di-*get* atau di-*set* variabelnya.
- *Class ModifyData* mempunyai *method* **updateName()**, yang membuat sebuah objek dari *class* Name dan mengakses instance variable-nya langsung dengan dot operator, karena variabel tersebut dideklarasikan **public**
- Karena itu, *method* **getName()** dan **setName()** tidak pernah dipanggil dan pengecekan akses keamanan mereka terlewati. Ini menunjukkan bahwa *class* Name *tighly coupled* dengan *class* **ModifyData**. Ini merupakan desain program yang jelek

#### **Example of Loose Coupling**
```java
class Names{
	private String name;
	
	public String getName(){
		//some code to check valid access to name
		return name;
	}
	
	public void setName(String s){
		//some code to check valid settingto name
		name=s;
	}
}

class ModifyData{
	public void updateName(){
		Name ob = new Name();
		ob.setName("Howard");
		ob.getName();
	}
}
```
Analisis Program:
- *Class* **Name** mempunyai instance variabel, name; yang dideklarasikan **private**.
- *Class Name* mempunyai 2 *public method* *getter, setter* yang memvalidasi akses ke variabel name sebelum di-*get* atau di-*set* variabelnya.
- *Class* **ModifyData** mempunyai *method* **updateName()**, yang membuat suatu objek dari *class* Name, lalu ia memanggil *method* **getName()** dan **setName()** dari *class* Name.
- Karena *method* **getName()** dan **setName()** dari *class* Name dipanggil dan 
pengecekannya berjalan sebelum instance variabelnya di-*get* atau di-*set*. Ini menunjukkan bahwa *class* **Name** *loose coupling* terhadap *class* **ModifyData**, dimana ini merupakan desain program yang baik.

Sumber: https://www.decodejava.com/coupling-cohesion-java.htm

## **Soal Tutorial: "Bisnis Bioskop"**
### **What's the story?**
Setelah bertualang membantu Kak Pewe mengembangbiakan kelincinya di RabbitHouse, membuat PEPERAS untuk Pak PeEl, dan menciptakan permainan Bingo bareng Sis Dea, kali ini Dek Depe ditawarkan oleh Koh Mas untuk merancang sistem penjualan tiket bioskop. Kebetulan sekali Dek Depe emang hobi nonton film maka tanpa pikir panjang Dek Depe langsung menyetujuinya. Karena Dek Depe tahu dia tidak bisa menyelesaikan pekerjaan ini sendirian, maka dia memanggil tim kepercayaannya, **JAVAvengers**, yang terdiri dari kalian sebagai programmer-programmer handal DDP2.
***
Berikut adalah permintaan dasar dari Koh Mas ke Dek Depe:

Program ini mensimulasikan sistem penjualan tiket pada bioskop-bioskop yang dimiliki oleh Koh Mas. Terdapat setidaknya 4 program yang wajib Anda buat (Anda dibebaskan untuk membuat lebih dari yang diminta apabila ada kebutuhan). Keempat program itu adalah program yang merepresentasikan **film, tiket film, bioskop, dan pelanggan**.
#### Film
Film memiliki atribut judul, rating umur (terdiri dari <span style="color:blue">"Umum"</span> untuk semua umur, <span style="color:blue">"Remaja"</span> untuk usia minimal 13 tahun, dan <span style="color:blue">"Dewasa"</span> untuk usia minimal 17 tahun), genre film, durasi dalam menit, dan status apakah film **lokal atau impor**. Program ini dapat mengembalikan hasil berupa informasi detail film dalam format seperti berikut:
```
------------------------------------------------------------------ 
Judul	: <Judul Film>
Genre	: <Nama Genre>
Durasi	: <Jumlah> menit
Rating	: <Umum/Remaja/Dewasa>
Jenis	: <Lokal/Impor>
------------------------------------------------------------------
```
#### Tiket
Selanjutnya adalah tiket. Tiket memiliki spesifikasi berupa film, jadwal tayang (hari), status apakah tiket biasa atau 3 dimensi. Program ini juga memiliki **harga tiket standar sebesar 60.000 Rupiah**. Program ini dapat menghitung harga tiket juga dimana harga tiket akan **bertambah sebesar 40.000 Rupiah** apabila tiket berupa hari **Sabtu** atau **Minggu**. Jika tiket tersebut berupa tiket jenis **3 Dimensi**, maka harga tiket **20 persen** lebih mahal daripada tiket biasa. Program ini juga dapat mencetak informasi tentang tiket dengan format:
```
------------------------------------------------------------------ 
Film			: <Judul Film>
Jadwal Tayang	: <Hari>
Jenis			: <Biasa/3 Dimensi>
------------------------------------------------------------------
```
#### Bioskop
Program Bioskop memiliki nama bioskop, daftar film yang dimiliki, daftar tiket film yang tersedia, dan saldo kas awal bioskop. Selain itu program ini juga bisa menyimpan **jumlah pendapatan total** dari gabungan semua bioskop. Dengan program ini, pelanggan dapat melakukan pembelian tiket dengan informasi judul film, jadwal film (hari), serta pilihan apakah film ditayangkan dalam 3 Dimensi atau 2 Dimensi. Program ini juga dapat menampilkan daftar film yang dimilikinya. Program juga dapat mencetak informasi singkat dengan format:
```
------------------------------------------------------------------ 
Bioskop			        : <Nama bioskop>
Saldo Kas		        : <Jumlah saldo>
Jumlah tiket tersedia	: <Jumlah tiket>
Daftar Film tersedia	: <Film-1>, <Film-2>, ..., <Film-N>
------------------------------------------------------------------
```
Koh Mas juga ingin agar program dapat mencetak jumlah saldo kas dari masing-masing bioskop serta saldo gabungan semuanya. Berikut format cetak yang diinginkan:
```
 Total uang yang dimiliki Koh Mas : Rp. <Jumlah pendapatan>
------------------------------------------------------------------
Bioskop			: <Nama bioskop-1>
Saldo Kas		: Rp. <Saldo-1>

Bioskop			: <Nama bioskop-2>
Saldo Kas		: Rp. <Saldo-2>

...

Bioskop			: <Nama bioskop-N>
Saldo Kas		: Rp. <Saldo-N>
------------------------------------------------------------------
``` 
#### Pelanggan
Terakhir adalah program yang merepresentasikan pelanggan. Data pelanggan yang perlu dicatat adalah nama, umur, dan jenis kelamin. Pelanggan dapat melakukan pembelian tiket di bioskop yang dia inginkan. 
- Setelah berhasil membeli tiket, program akan mencetak <pre>`<Nama pelanggan> telah membeli tiket <Nama Film> jenis <Biasa/3 Dimensi> pada hari <Hari> seharga Rp. <Harga tiket>`</pre> 
- Jika tiket yang ingin dibeli pelanggan tidak ada pada bioskop, maka cetak <pre>`Tiket untuk film <Judul Film> jenis <Biasa/3 Dimensi> dengan jadwal <Hari> tidak tersedia di <Nama Bioskop>`</pre> 
- Perlu diperhatikan juga, apabila umurnya tidak sesuai ketentuan, maka cetak <pre>`<Nama Pelanggan> masih belum cukup umur untuk menonton <Judul film> dengan rating <Umum/Remaja/Dewasa>`</pre> 
- Pelanggan juga dapat melihat informasi detail suatu film di bioskop yang dia pilih sesuai dengan [format](####Film) yang telah disebutkan di atas. Jika film yang dia cari tidak ada dalam bioskop, maka cetak <pre>`Film <Judul Film> yang dicari <Nama Pelanggan> tidak ada di bioskop <Nama Bioskop>`</pre>

## **Output yang diharapkan**
Berikut adalah output yang diharapkan ketika program utama dijalankan:
```
------------------------------------------------------------------
Bioskop					: CGT Blitz
Saldo Kas				: 0
Jumlah tiket tersedia	: 12
Daftar Film tersedia	: Black Panther, Dilan 1990, The Greatest Showman, Si Juki The Movie
------------------------------------------------------------------
------------------------------------------------------------------
Bioskop					: CompFest XXI
Saldo Kas				: 25000
Jumlah tiket tersedia	: 12
Daftar Film tersedia	: Black Panther, Dilan 1990
------------------------------------------------------------------
Tiket untuk film Black Panther jenis 3 Dimensi dengan jadwal Senin tidak tersedia di CGT Blitz
Dek Depe telah membeli tiket The Greatest Showman jenis 3 Dimensi di CGT Blitz pada hari Selasa seharga Rp. 72000
Film Si Juki The Movie yang dicari Sis Dea tidak ada di bioskop CompFest XXI
------------------------------------------------------------------
Judul	: Dilan 1990
Genre	: Romantis/Drama
Durasi	: 110 menit
Rating	: Remaja
Jenis	: Film Lokal
------------------------------------------------------------------
Tiket untuk film Dilan 1990 jenis Biasa dengan jadwal Selasa tidak tersedia di CompFest XXI
Sis Dea telah membeli tiket Dilan 1990 jenis Biasa di CompFest XXI pada hari Sabtu seharga Rp. 100000
Sis Dea masih belum cukup umur untuk menonton Si Juki The Movie dengan rating Dewasa
Kak Pewe telah membeli tiket Si Juki The Movie jenis Biasa di CGT Blitz pada hari Kamis seharga Rp. 60000
Kak Pewe telah membeli tiket Black Panther jenis 3 Dimensi di CompFest XXI pada hari Sabtu seharga Rp. 120000
Total uang yang dimiliki Koh Mas : Rp. 377000
------------------------------------------------------------------
Bioskop		: CGT Blitz
Saldo Kas	: Rp. 132000

Bioskop		: CompFest XXI
Saldo Kas	: Rp. 245000

------------------------------------------------------------------
```
Silakan implementasikan program-program yang diminta. **Program utama untuk menjalankan keempat program  tersebut sudah disediakan di dalam folder lab_6.** Tolong perhatikan penamaan class dan method yang digunakan dalam program utama (Lab6.java). 
## **Soal Bonus: "I want a refund!"**
### **What's the story?**
Demi memberikan pelayanan terbaik bagi pelanggannya, Koh Mas membuat kebijakan dimana pelanggan dapat membatalkan tiket **yang paling baru dibeli** namun belum digunakan untuk menonton film. Tiket yang dikembalikan tidak harus kepada bioskop yang sama dengan bioskop saat dia beli. Harga tiket juga akan dikembalikan meski pendapatan bioskop Koh Mas harus berkurang. 

- Setelah berhasil dikembalikan, maka cetak <pre>`Tiket film <Judul film> dengan waktu tayang <hari> jenis <Biasa/3 Dimensi> dikembalikan ke bioskop <Nama bioskop>`</pre> 
- Namun jika film di tiket tersebut tidak ada dalam daftar film yang tersedia dalam bioskop, maka cetak <pre>`Maaf tiket tidak bisa dikembalikan, <Judul film> tidak tersedia dalam <Nama bioskop>`</pre> 
- Jika tiket film sudah digunakan, maka cetak <pre>`Tiket tidak bisa dikembalikan karena film <Judul film> sudah ditonton oleh <Nama Pelanggan>`</pre> 
- Apabila saldo suatu bioskop tidak mencukupi untuk pengembalian duit tiket, maka cetak <pre>`Maaf ya tiket tidak bisa dibatalkan, uang kas di bioskop <Nama bioskop> lagi tekor...`</pre>

Selain itu, terdapat method `watchMovie` dengan parameter tiket yang akan ditonton. Ketika seorang pelanggan telah menggunakan tiketnya untuk menonton film, maka program akan mencetak:
`<Nama Pelanggan> telah menonton film <Judul Film>`

**Hint**: Pada program Film perlu mengimplementasikan fitur `equals` sendiri agar bisa memeriksa tiket film yang mau dikembalikan sama dengan film yang tersedia dalam bioskop. (Cari tahu mengenai overriding method)

## Contoh Output 
Berikut adalah contoh output yang diharapkan apabila program utama yang bagian bonus dijalankan
```
Tiket film Black Panther dengan waktu tayang Sabtu jenis 3 Dimensi dikembalikan ke bioskop CGT Blitz
Maaf tiket tidak bisa dikembalikan, Si Juki The Movie tidak tersedia dalam CompFest XXI
Kak Pewe telah menonton film Si Juki The Movie
Tiket tidak bisa dikembalikan karena film Si Juki The Movie sudah ditonton oleh Kak Pewe
Maaf ya tiket tidak bisa dibatalkan, uang kas di bioskop CGT Blitz lagi tekor...
Total uang yang dimiliki Koh Mas : Rp. 257000
------------------------------------------------------------------
Bioskop		: CGT Blitz
Saldo Kas	: Rp. 12000

Bioskop		: CompFest XXI
Saldo Kas	: Rp. 245000

------------------------------------------------------------------

```

## Checklist
Isi kurung siku komponen dengan x untuk menceklis komponen.
### Komponen Wajib | 100 Poin
- [ ] **Mengimplementasikan objek-objek yang diminta (boleh mengimplementasikan lebih dari yang diminta)**
- [ ] **Mengimplementasikan atribut-atribut yang sesuai pada setiap objek**
- [ ] **Mengimplementasikan setiap method sesuai dengan kebutuhan Koh Mas**
- [ ] **Menyimpan semua program selain program utama di package yang terpisah**

### Komponen Bonus | 10 Poin
- [ ] **Membuat fungsi pembatalan tiket (boleh menambahkan method pembantu jika merasa perlu)**
- [ ] **Membuat method watchMovie() untuk pelanggan untuk menandakan tiket sudah digunakan**
- [ ] **Membuat fungsi `equals` untuk mengecek kesamaan dua film**


-----
### **Woah, apa ini !?**

Ketika kalian meng-push hasil kerja kalian, kalian akan sadar bahwa ada logo cross merah atau centang hijau di samping hasil kerja kalian.

![alt text](https://i.imgur.com/ZNfetmP.png "Ilustrasi git 1")

Kalian mungkin memperhatikan bahwa kita mulai memakai sistem git sejak semester 2, mengikuti kakak angkatan kalian yang baru mulai memakai sistem git sejak semester 3. Salah satu guna dari menggunakan git adalah kita bisa menggunakan fitur Continuous Integration?

Apa itu Continuous Integration? Continuous Integration adalah konsep di mana ketika kalian push, hasil push kalian langsung di build (compile) dan di test (uji langsung). Jika gagal, kalian akan diberitahu.

Bagian Build baru akan dijelaskan di mata kuliah Advanced Programming. Kalian hanya perlu mengetahui bagian test.

Sistem yang digunakan untuk mengetest di Java bernama JUnit. Kita bisa menggunakan framework JUnit untuk mengetes secara langsung (tanpa harus print di command line). Untuk sekarang, kalian tidak perlu tahu cara kerja JUnit.

Kamu dapat memeriksa hasil kerja Junit di tab Commit. Tekan logo centang hijau atau cross merah untuk memeriksa detail lebih lanjut.

![alt text](https://i.imgur.com/E23AOfl.png "Ilustrasi commit")

Ketika kamu menekan logo tersebut, kamu akan memeriksa rangkuman dari tes tersebut yang memiliki dua lingkaran.

Jika lingkaran pertama cross, maka program kamu gagal karena compile error.
Jika lingkaran pertama centang hijau tetapi lingkaran kedua cross, maka program kamu tidak akurat.
Jika kedua lingkaran centang, berati program kamu sudah baik.

![alt text](https://i.imgur.com/1ElduFi.png "Ilustrasi status")

Kamu dapat menekan tombol cross merah atau centang hijau untuk melihat hasil lebih lanjut. Sebagai contoh, jika kalain mendapat cross merah di lingkaran kedua, kamu dapat menemkan cross merah kedua untuk melihat test case mana program kalian tidak akurat.
