/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */
import java.util.Scanner;

public class BingoCard {

	private Number[][] numbers;			//membuat instance variables dengan encapsulation
	private Number[] numberStates; 
	private boolean isBingo;
	
	public BingoCard(Number[][] numbers, Number[] numberStates) {
		this.numbers = numbers;			//membuat constructor
		this.numberStates = numberStates;
		this.isBingo = false;
	}
	//setters getters
	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

	public Number[] getNumberStates() {
		return numberStates;
	}

	public void setNumberStates(Number[] numberStates) {
		this.numberStates = numberStates;
	}	

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}
	//membuat method markNum
	public String markNum(int num){
		//jika angka yang diberikan tidak ada, akan mengeluarkan error
		if(this.numberStates[num] == null){
			return "Kartu tidak memiliki angka " + num;
		}
		//jika angka yang diberikan sudah tersilang, maka akan memberitahu
		else if(this.numberStates[num].isChecked()){
			return num + " sebelumnya sudah tersilang";
		}
		//jika dua kondisi di atas tidak terpenuhi, maka angka akan disilang
		else{
			int baris = this.numberStates[num].getX();
			int kolom = this.numberStates[num].getY();
			this.numbers[baris][kolom].setChecked(true);
			this.numberStates[num].setChecked(true);
			this.checkBingo();
			return num + " tersilang";
		}
	
	}	
	//membuat method info
	public String info(){
		String KartuBingo = "";
		for(int a = 0; a < 5; a++){
			KartuBingo += "|";
			for(int b=0; b<5; b++){
				if(this.numbers[a][b].isChecked()){
					KartuBingo = KartuBingo + " X  |";
				}
				//untuk merapikan kartu saat mengeluarkan perintah info
				else{
					if(this.numbers[a][b].getValue() >= 10){
						KartuBingo += (" " + this.numbers[a][b].getValue() +" |");
					}
					if(this.numbers[a][b].getValue() < 10){
						KartuBingo += ("  " + this.numbers[a][b].getValue() +" |");
					}
				}
			}
			//memberikan return/enter saat sebuah baris telah selesai
			if(a!=4)
				KartuBingo += "\n";
		}
		return KartuBingo;
	}
	//membuat method restart
	public void restart(){
		for(int i=0; i<5; i++){
			for(int j=0; j<5; j++){
				this.numbers[i][j].setChecked(false);
			}
		}
		for(int i=0; i<100; i++){
			if(this.numberStates[i] == null)
				continue;
			this.numberStates[i].setChecked(false);
		}
		System.out.println("Mulligan!\n");
	}
	//membuat method checkBingo dengan berbagai pattern Bingo yang ada
	public void checkBingo(){
		for(int x=0; x<5; x++){
			if(this.numbers[x][0].isChecked() && this.numbers[x][1].isChecked() && this.numbers[x][2].isChecked() && this.numbers[x][3].isChecked() && this.numbers[x][4].isChecked()){
				setBingo(true);
			}
			if(this.numbers[0][x].isChecked() && this.numbers[1][x].isChecked() && this.numbers[2][x].isChecked() && this.numbers[3][x].isChecked() && this.numbers[4][x].isChecked()){
				setBingo(true);
			}
			if(this.numbers[0][0].isChecked() && this.numbers[1][1].isChecked() && this.numbers[2][2].isChecked() && this.numbers[3][3].isChecked() && this.numbers[4][4].isChecked()){
				setBingo(true);
			}
			if(this.numbers[0][4].isChecked() && this.numbers[1][3].isChecked() && this.numbers[2][2].isChecked() && this.numbers[3][1].isChecked() && this.numbers[4][0].isChecked()){
				setBingo(true);
			}
		}
	}
}

//Jose Devian Hibono | 1706039603 | DDP-2 B/NN
